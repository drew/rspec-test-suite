require 'rails_helper'

describe Widget do
  describe '.create' do
    context 'with a name' do
      subject { Widget.create(name: 'name') }

      it { is_expected.to be_valid }
    end

    context 'without a name' do
      subject { Widget.create }

      it { is_expected.to be_invalid }
    end
  end
end
