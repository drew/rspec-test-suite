require 'rails_helper'

describe Bidget do
  describe '.create' do
    context 'with a name' do
      subject { Bidget.create(name: 'name') }

      it { is_expected.to be_valid }
    end

    context 'without a name' do
      subject { Bidget.create }

      it { is_expected.to be_invalid }
    end
  end
end
