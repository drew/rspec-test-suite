class CreateBidgets < ActiveRecord::Migration[6.0]
  def change
    create_table :bidgets do |t|
      t.string :name

      t.timestamps
    end
  end
end
