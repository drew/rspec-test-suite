class BidgetsController < ApplicationController
  before_action :set_bidget, only: [:show, :update, :destroy]

  # GET /bidgets
  def index
    @bidgets = Bidget.all

    render json: @bidgets
  end

  # GET /bidgets/1
  def show
    render json: @bidget
  end

  # POST /bidgets
  def create
    @bidget = Bidget.new(bidget_params)

    if @bidget.save
      render json: @bidget, status: :created, location: @bidget
    else
      render json: @bidget.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /bidgets/1
  def update
    if @bidget.update(bidget_params)
      render json: @bidget
    else
      render json: @bidget.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bidgets/1
  def destroy
    @bidget.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bidget
      @bidget = Bidget.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bidget_params
      params.require(:bidget).permit(:name)
    end
end
